module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Furious CSS',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Furious SCSS' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#05a1d3' },
  /*
  ** Global Style
  */
  css: [
    '@/assets/styles.scss'
  ],
  /*
  ** Plugins
  */
  plugins: [
    {
      src: './plugins/snotify',
      ssr: false
    },
    {
      src: './plugins/clipboard'
    },
    {
      src: './plugins/tooltip'
    },
    {
      src: './plugins/vuetify',
      ssr: false
    },
    {
      src: './plugins/clickOutside',
      ssr: false
    }
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.resolve.alias['vue'] = 'vue/dist/vue.common'
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
